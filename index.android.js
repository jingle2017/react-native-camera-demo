/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  NativeModules
} from 'react-native';

export default class HeadImage extends Component {
  render() {
    return (
      <View style={styles.container}>
       <TouchableOpacity onPress={this._clickImage}>
	   <Image source={{uri:'ic_launcher'}} style={{width:50,height:50}}/>
	   </TouchableOpacity>
      </View>
    );
  }
  
  _clickImage(){
	  console.log("click image=====");
	  var savePath="/debug/"
	 NativeModules.HeadImageModule.callCamera(savePath);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('HeadImage', () => HeadImage);
