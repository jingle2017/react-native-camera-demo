package com.headimage;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Log;

import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;

import static com.facebook.react.common.ReactConstants.TAG;

/**
 * Created by Administrator on 2017/3/27.
 */

public class HeadImageModule extends ReactContextBaseJavaModule {

    // startActivityForResult 的 requestCode
    private static final int REQUEST_CODE_CAMERA = 0;
    private static final int REQUEST_CODE_GALLERY = 1;
    private static final int REQUEST_CODE_CROP = 2;
	private static String SAVE_FILE_IMAGE_PASH=null;
    public HeadImageModule(ReactApplicationContext reactContext) {
        super(reactContext);
        reactContext.addActivityEventListener(new BaseActivityEventListener(){
            @Override
            public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
                Log.d("zhu","========zhu=========");

                    if(resultCode==Activity.RESULT_OK){
                        switch (requestCode){
                            case 1:
                                Log.d("zhu","========zhu1=========");
                                saveCameraImage(data,SAVE_FILE_IMAGE_PASH);
                                break;
                            default:
                                Log.d("zhu","========zhu2=========");
                                break;
                        }
                    }
                }
        });
    }
    //保存相機圖片
    private void saveCameraImage(Intent data,String saveSDPath) {
        // 檢查SD卡是否存在！
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Log.d("zhu", "sd card is not avaiable/writeable right now.=========");
            return;
        }else{
            Log.d("zhu", "SD卡存在=========");
        }
        //為圖片命名
        String name = new DateFormat().format("yyyymmdd",
                Calendar.getInstance(Locale.CHINA))
                + ".jpg";
        Bitmap bmp = (Bitmap) data.getExtras().get("data");// 解析返回圖片bitmap

        // 保存文件
        FileOutputStream fos = null;
        File file = new File(saveSDPath);
        file.mkdirs();// 創建文件夾
        String fileName = saveSDPath + name;// 保存路徑

        try {// 写入SD card
            fos = new FileOutputStream(fileName);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                fos.flush();
                fos.close();
				bmp=null;//Bitmap對象占內存比較大，所以一般最後會置空
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }// 显示图片
    }

    @Override
    public String getName() {
        return "HeadImageModule";//注意這裡的返回值
    }

    @ReactMethod
   public void callCamera(String savePath) {//調用相機的方法
        Log.d("zhu", "call camera==============");
		SAVE_FILE_IMAGE_PASH=Environment.getExternalStorageDirectory()+savePath;
        //調用系統相機
        Activity activity=getCurrentActivity();
        if(activity!=null){
            activity.startActivityForResult(new  Intent(MediaStore.ACTION_IMAGE_CAPTURE),REQUEST_CODE_GALLERY);
        }
    }


}

    
